from urllib.request import urlopen
import xml.etree.ElementTree as ET
import os
import requests
from tqdm import tqdm


def download_from_url(url, dst='base_download/fias_downtest'):
    """
        Функция загрузки файлов
        :param url: урл для загрузки
        :param dst: файл
        :return:
    """
    file_size = int(urlopen(url).info().get('Content-Length', -1))
    if os.path.exists(dst):
        first_byte = os.path.getsize(dst)
    else:
        first_byte = 0
    if first_byte >= file_size:
        return file_size
    header = {"Range": "bytes=%s-%s" % (first_byte, file_size)}
    pbar = tqdm(
        total=file_size, initial=first_byte,
        unit='B', unit_scale=True, desc=url.split('/')[-1])
    req = requests.get(url, headers=header, stream=True)
    with(open(dst, 'wb')) as f:
        for chunk in req.iter_content(chunk_size=1024):
            if chunk:
                f.write(chunk)
                pbar.update(1024)
    pbar.close()
    return file_size


if __name__ == '__main__':
    url_db = 'http://fias.nalog.ru/Public/Downloads/Actual/fias_delta_dbf.rar'
    file_name = 'fias_delta_dbf.rar'
    size = download_from_url(url_db, file_name)
    print('Файл {} размером {} байт был загружен'.format(file_name, size))
